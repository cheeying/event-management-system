<?php
	//Connect database
	include "database/connectdb.php";

	// //Read session
	include 'session.php';
	// $uid=$_SESSION['UserID'];
	// if($uid=='' || $uid==null){
	// 	$message="Please login to continue";
	// 	echo "<script type='text/javascript'>alert('$message');</script>";
	// 	header("Refresh: 0, login_register.php");
	// }

	// //Read button script
	// include "top_button.html";
	
?>
<!DOCTYPE html>
<html>
	<head>
        <title>All Events - Edit Event</title>

		<!-- CSS -->
		<link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
		
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script> 
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" integrity="sha512-63+XcK3ZAZFBhAVZ4irKWe9eorFG0qYsy2CaM5Z+F3kUn76ukznN0cp4SArgItSbDFD1RrrWgVMBY9C/2ZoURA==" crossorigin="anonymous" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg==" crossorigin="anonymous"></script>

		<!-- Script -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
		<script src='select2/dist/js/select2.min.js' type='text/javascript'></script>

		<style type="text/css">
		body{
			width: 99%;
			height:98%;
			/*PUSH9*/
			/* color:#457888; */
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}
		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}
		a:hover {
			color: lightgrey;
			text-decoration: none;
		}

		a {
			color: white;
			text-decoration: none;
		}
			.container {
				margin-top: 100px;
			}
			.btn-primary {
				width: 100%;
			}
		</style>
		
		
        <script type="text/javascript">
		
                $(document).ready(function() {
					// $('#datetimepicker1').datetimepicker();
					$("#a_eventname").select2();

                    $('#a_eventname').on('change',function() {
                        var sname = $(this).val();
						var data_String = 'sname=' + sname;
						var date = new Date();
            			var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                        $.get('search.php', data_String, function(result) {
                            $.each(result, function(){
								$('#a_eventname').val(this.evt_name);
								$('#picture').attr("src",'uploads/'+this.evt_imagename);
								$('#a_datetime').val(this.evt_datetime);	
								$('#a_eventcategory').val(this.evt_cat);
                                $('#a_eventdescription').val(this.evt_description);
								$('#a_eventvenue').val(this.venue_name);
                                $('#a_eventticketprice').val(this.tkt_price);
								$('#a_eventtickettotal').val(this.tkt_total);
								
                            });			
                        });
					});
					
                });
		</script>

		<script>
			$(document).ready(function(){
				$("#deleteevent").click(function(){
					location.reload(true);
				});
			});
		</script>
		
		
		<script type='text/javascript'>
			$( document ).ready(function() {
				var date = new Date();
            	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
				$('#datetimepicker1').datetimepicker({
					// date_format: 'yyyy-mm-dd hh:ii',
					format: 'YYYY-MM-DD HH:mm',
					minDate: today
				});
			});
		</script>
		
	</head>
	<div class="wrapper">
		<div class="row">
			<div class="col-md-10 col-md-push-1 col-lg-6 col-lg-push-3">
				<div class="box box-default" align="center">
					<div class="box-body">
						<div id="edit">
							<form action="event-manage-edit.php" method="POST" name='form1' enctype="multipart/form-data">
								<fieldset>
									<legend>
									>>> Event Edit <<<
									</legend>
									<div class="card-content">
										<table>
											<thead>
												<tr>
													<th>Event Title:</th>
													<th><select name="a_eventname" id="a_eventname" size="4" >
														<?php
															$conn = mysqli_connect($servername, $username, $password, $dbname);
															$read_venue = "SELECT * FROM event";
															$result_read_venue = mysqli_query($conn, $read_venue);
															if(mysqli_num_rows($result_read_venue)>0){
																while($row = mysqli_fetch_array($result_read_venue, MYSQLI_ASSOC)){
																	echo "<option value='".$row['evt_name']."'>".$row['evt_name']."</option>";
																}
															}
														?>
													</select></th>
												</tr>
											</thead>
										
											</td></tr>

												<tbody>
													<tr>
														<td><label style='margin-top: 20%'>Event Image:</label></td>
														<td><input height="200" width="290" img src="" id="picture" name="picture" type="image" style='margin-top: 7%'/></input></td>
															<input type="file" id="fileToUpload" name="fileToUpload" style="display: none;"/></input>
															<script>
																$("input[type='image']").click(function(e) {
																	e.preventDefault();	
																	$("input[id='fileToUpload']").click();
																});
															</script>
															<script>
																function readURL(input) {
																	if (input.files && input.files[0]) {
																		var reader = new FileReader();
																		
																		reader.onload = function(e) {
																		$('#picture').attr('src', e.target.result);
																		}
																		
																		reader.readAsDataURL(input.files[0]); // convert to base64 string
																	}
																}
																$('#fileToUpload').change(function() {
																	// $('picture').attr('src', './path/to/new/image');
																	readURL(this);
																});
															</script>
															<!-- <input id= "fileToUpload" class="d-block" type="file" name="fileToUpload" size="20"></input> -->
														</td></tr>
													<tr>
														<td><label style='margin-top: 20%'>Event Date&Time:</label></td>
														<td><div class='input-group date' id='datetimepicker1' style="margin-top: 7%">
														<input id = 'a_datetime'name ='a_datetime' type='text' class="form-control" size="27" />
														<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
														</span>
														</div>
														</td>
													</tr>
													<tr>
														<td><label style='margin-top: 20%'>Event Category:</label></td>
														<td><textarea name="a_eventcategory" id="a_eventcategory"  rows="2" cols="50" placeholder="eg: Concert, Sports, Talk, Festival etc..." required  style="margin-top: 7%"></textarea>
														</td>
													</tr>
													<tr>
														<td><label style='margin-top: 20%'>Event Description:</label></td>
														<td><textarea name="a_eventdescription" id="a_eventdescription" rows="5" cols="50" required style="text-align: justify; margin-top: 7%" ></textarea>
														</td>
													</tr>
													<tr>
														<style type="text/css">     
															select {
																width:280px;
															}
														</style>
														<td><label style='margin-top: 20%'>Venue:</label></td>
														<td><select name="a_eventvenue"  id="a_eventvenue"  style="margin-top: 7%" >
														<?php
															$conn = mysqli_connect($servername, $username, $password, $dbname);
															$read_venue = "SELECT * FROM venue";
															$result_read_venue = mysqli_query($conn, $read_venue);
															if(mysqli_num_rows($result_read_venue)>0){
																while($row = mysqli_fetch_array($result_read_venue, MYSQLI_ASSOC)){
																	echo "<option value='".$row['v_name']."'>".$row['v_name']."</option>";
																}
															}
														?>
														</select></td>
													</tr>
													<tr>
														<td><label style='margin-top: 20%'>Ticket Price: RM </label></td>
														<td><input type="number" name="a_eventticketprice" id="a_eventticketprice" min="00" placeholder="0" size="35" required  style="margin-top: 7%">.00 </td>
													</tr>
													<tr>
														<td><label style='margin-top: 20%'>Number of Ticket: </label></td>
														<td><input type="number" name="a_eventtickettotal" id="a_eventtickettotal" min="10" placeholder="10" size="35" required  style="margin-top: 7%"></td>
													</tr>
												</tbody>
										</table>
									</div>
								</fieldset>
								<div class="text-center">
									<!-- <button type="submit" id="item_save" name="save" class="btn btn-success"><i class="fa fa-check"></i>&nbsp; Save</button> -->
									<!-- <button type="submit" id="item_saveAndAdd" name="item[saveAndAdd]" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp; Save &amp; Add Another</button> -->
									<tr><td colspan="2"><input type="submit" name="addevent" value="Save"  style="margin-top: 4%">&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="submit" name="deleteevent" value="Delete">&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="reset" name="cancel" value="Cancel"  style="margin-top: 4%">&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<input type="button" onclick="location.href='http://localhost/event-management-system/mail_form.php';" value="Notify users" /></tr>
									
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		if (isset($_POST['addevent'])) {
			$ename=$_POST['a_eventname'];
			$gettime=$_POST['a_datetime'];
			$converttime=strtotime($gettime);
			$timeformat=date("Y-m-d H:i:s",$converttime);
			$edatetime=$timeformat;
			$edescription=$_POST['a_eventdescription'];
			$ecategory=$_POST['a_eventcategory'];
			$evenue=$_POST['a_eventvenue'];
			$eprice=$_POST['a_eventticketprice'];
			$etotal=$_POST['a_eventtickettotal'];

			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$name =addslashes($_FILES['fileToUpload']['name']);
			$image=addslashes($_FILES['fileToUpload']['tmp_name']);
			if($image!=""){
				$image= file_get_contents($image);
				$image= base64_encode($image);
			}

			//Read venue id
            $read_venue_id="SELECT venueID FROM venue WHERE v_name='$evenue'";
            $read_event_id="SELECT eventID FROM event WHERE evt_name='$ename'";
            $result_read_event_id=mysqli_query($conn, $read_event_id);
            $result_read_venue_id = mysqli_query($conn, $read_venue_id);
            if($result_read_event_id){
                if($result_read_venue_id){
                    while($row=mysqli_fetch_array($result_read_event_id, MYSQLI_ASSOC)){
                        $eid=$row['eventID'];
                        while($row = mysqli_fetch_array($result_read_venue_id, MYSQLI_ASSOC)){
							
							move_uploaded_file($_FILES['fileToUpload']['tmp_name'],"uploads/$name");

                            $vid=$row['venueID'];
							//Insert event
							if($name!=""){
									$insert_event = "UPDATE event SET evt_name='$ename',evt_datetime='$edatetime',evt_cat='$ecategory',evt_description='$edescription', tkt_price='$eprice', tkt_total='$etotal', evt_image='$image', evt_imagename='$name', venue_name='$evenue', venueID='$vid' WHERE eventID='$eid'";
							}
							else{
								$insert_event = "UPDATE event SET evt_name='$ename', evt_datetime='$edatetime' , evt_cat='$ecategory', evt_description='$edescription', tkt_price='$eprice', tkt_total='$etotal', venue_name='$evenue', venueID='$vid' WHERE eventID='$eid'";
							}
                            $result_insert_event = mysqli_query($conn, $insert_event);
                            if($result_insert_event){
                                $message="Edit event success.";
                                echo "<script type='text/javascript'>alert('$message');</script>";
                            }
                            else{
                                $message="Fail to edit event. Please try again.";
                                echo "<script type='text/javascript'>alert('$message');</script>";
                            }
                        }
                    }
                }
            }
		}
if (isset($_POST['deleteevent'])) {
			$selectname=$_POST['a_eventname'];
			//Read event id
			$read_event_id = "SELECT eventID FROM event WHERE evt_name='$selectname'";
			$result_read_event_id = mysqli_query($conn, $read_event_id);
			if($result_read_event_id){
				while($row = mysqli_fetch_array($result_read_event_id, MYSQLI_ASSOC)){
					$eid=$row['eventID'];
					//Check if any booking was made
					//If one or more booking found, delete fail
					$check_booking="SELECT eventID FROM booking WHERE EventID='$eid'";
					$result_check_booking = mysqli_query($conn, $check_booking);
					if(mysqli_num_rows($result_check_booking)>0){
						$message="Fail to delete event. One or more booking found.";
						echo "<script type='text/javascript'>alert('$message');</script>";
					}
					else{
						$delete_event = "DELETE FROM event WHERE eventID='$eid'";
						$result_delete_event = mysqli_query($conn, $delete_event);
						if($result_delete_event){
							$message="Delete event success.";
							echo "<script type='text/javascript'>alert('$message');</script>";
							echo "<meta http-equiv='refresh' content='0'>";
						}
						else{
							$message="Fail to delete event. Please try again.";
							echo "<script type='text/javascript'>alert('$message');</script>";
							echo "<meta http-equiv='refresh' content='0'>";
						}
					}
				}
			}
		}
		?>
</html>
