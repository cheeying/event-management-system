<?php
	//Connect database
	include "database/connectdb.php";

	//Read session
    include 'session.php';
    
	$name=$_SESSION['name'];
	if($name=='' || $name==null){
        $message="Please login to continue";
		echo "<script type='text/javascript'>alert('$message');</script>";
		header("Refresh: 0, registerlogin.php");
	}
?>

<!DOCTYPE html>
<html>
<head>	
	<title>Notifications</title>
	<style type="text/css">
		body{
			font-family: Arial;
  			font-size: 17px;
			width: 98%;
			height:100vh;
			/* color:#457888; */
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}

		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}
        a:hover{
			color:lightgrey;
		}
		a{
			color:  white;
			text-decoration: none;
		}
        .top{
			font-size: 34px;
			font-family: Helvetica;
			text-align: center;
            border-style: solid;
            border-width: 15px;
            border-color: white; 
            color: white;
            background-color: transparent;
            /* padding:10px; */
		}
		form{
			margin-left: 60px;
			margin-top: 15px;
			margin-right: 60px;
		}
		table{
			max-width: 1200px;
            width: 100%;
			margin-bottom:20px;
			margin-left:auto;
			margin-right:auto;
            /* border:5px solid white; */
			/* background-color: white; */
			text-align:center;
			padding-top: 10px;
            padding-bottom: 10px;
			padding-left: 20px;
			padding-right: 20px;
		}
		th{
			background-color: #EFDEF0;
			/* border:1px solid white; */
			font-size: 20px;
			text-align: center;
			padding: 5px 10px;
		}
		td{
			/* border:1px solid black; */
			font-size: 18px;
			/* font-family: Times New Roman; */
			padding: 5px 5px;
            background-color: white;
		}
		div{
			margin: auto;
			padding-bottom: 5px;
			min-width: 50%;
			max-width: 80%;
			/* background-color: white; */
		}
		hr{
			border-top: 5px dotted grey;
			border-bottom: none;
			border-left: none;
			border-right: none;
			width: 95%;
			padding-top: 10px
		}
	</style>
</head>
<body>
    <div class="top">
            <h1>Notification</h1>
    </div>


	<div id="view" align="center">
        <br>

		<table align="center" cellpadding="15px" cellspacing="7px">
        <!-- <tr><th colspan = 5 style="font-size: 30px; font-weight: 1000;">My Booking</th></tr> -->
        <br>
			<tr>
				<th>No.</th>
				<!-- <th>Booking<br>Date & Time</th> -->
				<th>Event Name</th>
				<!-- <th>Event<br>Date & Time</th> -->
				<!-- <th>Event Time</th> -->
				<th>Message</th>
                <th>Announced At</th>
			</tr>
			<!--Get all notification record of hte user-->
			<?php
				
				$count=0;
				$conn = mysqli_connect($servername, $username, $password, $dbname);
				//Read user notification detail
				$read_user_notification = "SELECT notification.*,event.evt_name FROM notification join event on notification.eventID = event.eventID order by notification.datetime desc";
				$result_read_user_notification = mysqli_query($conn, $read_user_notification);
				if ($result_read_user_notification){
					while($row = mysqli_fetch_array($result_read_user_notification, MYSQLI_ASSOC)){
						$eid=$row['eventID'];
						$count=$count+1;
						echo "<tr>";
						echo "<td>".$count."</td>";
						echo "<td>".$row['evt_name']."</td>";
                        echo "<td style='text-align:left; min-width:150px; max-width:300px; padding-left:10px'>".$row['message']."</td>";
                        echo "<td>".$row['datetime']."</td>";
						echo "</tr>";
					}
				}
			?>
		</table>
	</div>
</body>
</html>