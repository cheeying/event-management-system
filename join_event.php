<?php
//Connect database
include "database/connectdb.php";

//Read session
include 'session.php';

//Join any event
if (isset($_POST['join'])) {
	//Go login page if not login
	if ($login_status == "no") {
		$message = "Please login to continue.";
		echo "<script type='text/javascript'>alert('$message');</script>";
		header("Refresh: 0; registerlogin.php");
	}
	
//Set Event ID
if (isset($_POST['join'])) {
	$eventID = $_POST['evntID'];
}

//Set Quantity
if (isset($_POST['submit'])) {
	$eventID = $_POST['evntID'];
	$quantity = $_POST['quantity'];
}
	//Purchase ticket to join event, Only ONE booking per user
	else if ($login_status == "yes") {

		//header("Refresh: 0; payment.php");
		// $ename=$_POST['joineventname'];
		// $event = [];

		// //Read selected event ID
		// $conn = mysqli_connect($servername, $username, $password, $dbname);

		// //Read related event
		// $read_DB = "SELECT * FROM event WHERE event.eventID = $eventID";

		// // $read_DB = "SELECT * FROM event WHERE eventID = $eventID";
		// $result = mysqli_query($conn, $read_DB);
		// if(mysqli_num_rows($result)>0){
		// 	$event = $result->fetch_assoc();
		// }


		// //Check if user purchased ticket for the event
		// $read_book_record = "SELECT * FROM booking WHERE name='$uname' AND eventID='$eventID'";
		// $result_read_book_record = mysqli_query($conn, $read_book_record);
		// $number_of_rows = mysqli_num_rows($result_read_book_record);
		// // //If purchased
		// // if($number_of_rows>0){
		// // 	$message="You have already purchased for this event. Please insert the number of tickets you want to purchased: ";
		// // 	echo "<script type='text/javascript'>alert('$message');</script>";
		// // 	header("Refresh: 0; index.php");
		// // }
		// // //If not purchase, check ticket availability
		// // else{
		// 	$read_ticket_info = "SELECT tkt_total, tkt_sold from event WHERE eventID='$eventID'";
		// 	$result_read_ticket_info = mysqli_query($conn, $read_ticket_info);
		// 	if($result_read_ticket_info){
		// 		while($row = mysqli_fetch_array($result_read_ticket_info, MYSQLI_ASSOC)){
		// 			$tickettotal=$row['tkt_total'];
		// 			$ticketsold=$row['tkt_sold'];
		// 			//If ticket sold is equal to total number of ticket, booking fail
		// 			if($ticketsold==$tickettotal){
		// 				$message="Oh No! Tickets have been sold out! So sorry to inform that!";
		// 				echo "<script type='text/javascript'>alert('$message');</script>";
		// 			}
		// 			//Else, update ticket sold, then insert booking detail
		// 			else{
		// 				$currentsoldticket = $ticketsold+1;
		// 				$update_ticket_sold = "UPDATE event SET tkt_sold=$currentsoldticket WHERE eventID='$eventID'";
		// 				$result_update_ticket_sold = mysqli_query($conn, $update_ticket_sold);
		// 				if($result_update_ticket_sold){
		// 					date_default_timezone_set('Asia/Kuala_Lumpur');
		// 					$current_time = date('Y-m-d H:i:s');
		// 					$insert_booking = "INSERT INTO booking (bk_timestamp, name, eventID) VALUES ('$current_time', '$uname', '$eventID' ) ";
		// 					$result_insert_booking = mysqli_query($conn, $insert_booking);
		// 					if($result_insert_booking){
		// 		    			$message="Ticket purchased successfullly.";
		// 						echo "<script type='text/javascript'>alert('$message');</script>";	
		// 					}
		// 					else{
		// 						$message="Fail to purchase ticket. Please try again.";
		// 						echo "<script type='text/javascript'>alert('$message');</script>";
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }
	}
}
?>
<!DOCTYPE html>
<html>

<head>
	<title>UNIVERSITY Events - Join Event</title>
	<style>
		body {
			width: 98%;
			height: 100vh;
			/* color:#457888; */
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}

		@keyframes change {
			0% {
				background-position: 0 50%;
			}

			50% {
				background-position: 100% 50%;
			}

			100% {
				background-position: 0 50%;
			}
		}

		a:hover {
			font-size: 24px;
		}

		a {
			color: blue;
		}

		.top {
			font-size: 34px;
			width: 80%;
			margin: auto;
			font-family: Helvetica;
			text-align: center;
			border-style: solid;
			border-width: 15px;
			border-color: white;
			color: white;
			/* padding:10px; */
		}

		input[type=submit] {
			padding: 12px;
			color: black;
			border: none;
			background-color: #66CDAA;
			font-weight: 900;
			font-family: Times New Roman;
			font-size: 16px;
			text-align: center;
			width: auto;
		}

		input[type=submit]:hover {
			background-color: #20B2AA;
		}

		form {
			margin-left: 60px;
			margin-top: 15px;
			margin-right: 60px;
		}

		table {
			margin-left: auto;
			margin-right: auto;
			width: 80%;
			padding: 20px;
			text-align: justify;
			background-color: white;
		}

		th {
			padding-bottom: 20px;
		}

		.event_name {
			font-family: Times New Roman;
			border-style: none;
			font-size: 30px;
			margin-top: 10px;
		}
	</style>
</head>

<body>

	<div class="top">
		<h1>INTI EVENTS</h1>
	</div>
	<div class="content" align="center">
		<?php



		$conn = mysqli_connect($servername, $username, $password, $dbname);

		//Read related event
		$read_DB = "SELECT * FROM event INNER JOIN venue ON event.venueID = venue.venueID  WHERE event.eventID = $eventID";

		// $read_DB = "SELECT * FROM event WHERE eventID = $eventID";
		$result = mysqli_query($conn, $read_DB);
		//Display related result and details
		if (mysqli_num_rows($result) > 0) {
			echo "<form action='payment.php' method='POST'>";
			while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
				$datetime = $row['evt_datetime'];
				$date = date('Y-m-d', strtotime($datetime));
				$time = date('H:i:s', strtotime($datetime));
				$ticketsold = $row['tkt_sold'];
				$totalticket = $row['tkt_total'];
				$availableticket = $totalticket - $ticketsold;
				echo "<table style='position:relative;'>";
				echo "<tr><th colspan='2'><span style='border-bottom:7px solid black'><input type='hidden' id='evntID' name='evntID' value='" . $eventID . "'><input class ='event_name'  type='text' name='joineventname' value='" . $row['evt_name'] . "' size=80 readonly></th></tr>";
				echo "<tr><td rowspan='2' width='30%' style='text-align:left;vertical-align:top;padding:0'><span style='width:30px'><img src=uploads/" . $row['evt_imagename'] . " style=' display:block; width:100%; height:auto;'/></td>";
				echo "<td width='70%' style='text-align:justify;vertical-align:top;padding:0 0 0 1rem;'><span  style='font-size:16px'>" . $row['evt_description'] . "</td></tr>";
				echo "<tr>
        					<td>
        						<ul>
        							<li><b>Date: </b>" . $date . "</li>
        							<li><b>Time: </b>" . $time . "</li>
									<li><b>Price: </b>RM " . $row['tkt_price'] . "</li>
									<li><b>Ticket Available: </b>" . $availableticket . "</li>
									<li><b>Quantity: </b>
									<input type='number' id='quantity' name='quantity' value='1' min='1' max='" . $availableticket . "' size='6'></input></li>
								</ul></span></td></tr>";



				echo "<tr><td style='height:50px;padding-bottom:30px;'><input type='submit' name='passeventid' style='left:50%;position:absolute;bottom:25px' value='Buy Ticket' ></input></td></tr>";
				echo "</table><br>";
			}
			echo "</form>";
		} else {
			echo "no data";
		}
		?>
			<!--<script>
	setTimeout(function(){
		window.location.href = "/event-management-system/master/booking.php";
	},1000)
	</script>-->
</body>

</html>