<?php
    //Connect database
	include "database/connectdb.php";
	
	//Read session
	include 'session.php';

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Event Ticket Management System</title>

        <style type="text/css">
		body{
			font-family: Arial;
  			font-size: 17px;
			width: 98%;
			height:100vh;
			/* color:#457888; */
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}

		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}

		a:hover{
			color:lightgrey;
		}
		a{
			color:  white;
			text-decoration: none;
		}
		.top{
			font-size: 34px;
			width: 80%;
			margin: auto;
			text-align: center;
            border-style: solid;
            border-width: 15px;
            border-color: white; 
            color: white;
            /* padding:10px; */
		}
		form{
			margin-left: 60px;
			margin-top: 15px;
			margin-right: 60px;
		}
		input[type=submit]{
			padding: 10px;
			color: black;
			border: none;
			background-color: #66CDAA;
			font-weight: 800;
			font-size: 14px;
			text-align: center;
			width: auto;
		}
		input[type=submit]:hover{
			background-color: #20B2AA;
		}
		table{
			margin-left:auto;
			margin-right:auto;
			max-width: 70%;
			padding:20px;
			text-align:justify;
			background-color: white;
		}
		th{
			padding-bottom:20px;
		}
		.event_name{
			border-style: none;
			font-size: 30px;
			margin-top: 10px;
		}
	    </style>
    </head>

    <body>
        <!--<button onclick=""></button>-->
        <div class="top">
            <h1>INTI EVENTS</h1>
		</div>
		<br>

        <!--Display all events-->
        <div class="content" align="center">
            <!--function-->
            <?php
                $conn = mysqli_connect($servername, $username, $password, $dbname);

                //Read events
                $read_DB = "SELECT * FROM event ORDER BY evt_datetime DESC";
                $result = mysqli_query($conn, $read_DB);
                
                //Display results from database
                if($result){
                    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                        echo "<form action='event_detail.php' method='POST'><table style='position:relative;'>";
                        echo "<tr><th colspan='2'><span style='border-bottom:7px solid black'><input class ='event_name'  type='text' name='eventname' value='".$row['evt_name']."' size=80 readonly><input type='hidden' name='eventID' value='".$row['eventID']."'></th></tr>";
                        echo "<tr><td rowspan='2' width='30%' style='text-align:left;vertical-align:top;padding:0'><span style='width:30px'><img src=uploads/".$row['evt_imagename']." style='display:block; width:100%; height:auto;'/></td>";
                        echo "<td width='70%' style='text-align:justify;vertical-align:top;padding:0 0 0 1rem;'><span  style='font-size:16px'>".$row['evt_description']."</td></tr>";
                        echo "<tr><td style='height:50px;padding-bottom:30px;'><input type='submit' name='more_detail' style='left:50%;position:absolute;bottom:25px' value='More Details'/></td></tr>";
                        echo "</table></form><br>";
                    }
                }
            
            ?>
        </div>
    </body>
</html>
