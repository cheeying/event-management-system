<!DOCTYPE html>
<html>
    <head>
        <title>Successfully Logout</title>
    </head>
    <body>
        <?php
        session_start();
        if(isset($_SESSION['name'])){
            session_destroy();
            echo "<h1>You have logout successfully.</h1>";
            header('Refresh: 1; index.php');
        }
        ?>
    </body>    
</html>