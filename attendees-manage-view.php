<?php
    include_once "database/connectdb.php";
    include 'session.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>ETMS - View Attendees</title>

	<style type="text/css">

		.attendees_title{
				font-weight: 900;
				font-size: 55px;
				font-family: Helvetica;
				text-align: center;
				color: white;
		}
		a:hover {
			color: lightgrey;
		}

		a {
			color: white;
			text-decoration: none;
		}
		form{
			margin-left: 60px;
			margin-top: 40px;
			margin-right: 60px;
		}
		table{
			max-width:1200px;
			margin-bottom:50px;
			margin-left:auto;
			margin-right:auto;
			background-color: none;
			text-align:center;

			border-style: solid;
			border-width: 5px;
			border-color: white;
		}
		th{
			background-color: none;
			border:5px solid #FFFFFF;
			font-size: 30px;
			font-family: Helvetica;
			text-align: center;
			padding-top: 10px ;
			padding-bottom: 10px ;
		}
		td{
			border:2px solid white;
			font-size: 28px;
			font-family: Helvetica;
			text-align: center;
			padding-top: 5px ;
			padding-bottom: 5px ;
		}
		input[type=submit]{

			color: black;
			border: none;
			background: transparent;
			font-weight: 700;
			font-family: Helvetica;
			font-size: 20px;
			text-align: center;
			cursor: pointer;

		}

		input[type=submit]:hover, input[type=button]:hover{
			background-color: none;
			box-shadow: 0 5px none;
			transform: translateY(4px);
		}
		

		div{
			margin: auto;
			margin-top: 50px;
			padding-bottom: 1px;
			width: 70%;
			background-color: none;
		}

		body{
			width: 99%;
			height: 98%;
			/* colorful color:#457888; */ 
			font-family: Arial;
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}

		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}


		
	</style>
</head>
<body background="src\loginbackground.jpg">

	<!-- <button onclick="topFunction()" id="myBtn" title="Go to top"></button> -->

	<!--Sort according to name in ascending/descending order-->
	<!--Sort according to UserID by default-->
	<div id="view" align="center">
		<br>
		<p><span style="text-decoration: underline;font-weight: 900;font-size: 55px">  Attendees List </span></p>
		<form action="attendees_manage_view.php" method="POST" style="font-size: 20px;">
		<table align="center" cellpadding="20px" cellspacing="6px">
			<tr>
				<th>No.</th>
				<th>Name <input type="submit" class ="a_btn" name="ascending" value="▲"><input type="submit" class="d_btn" name="descending" value="▼"></th>
				<th>E-mail</th>
				<th>Date Registered</th>
				<th>Ticket Bought</th>
			</tr>
			</form>
			<?php
				$searchkey='-';

				if(isset($_POST['attendeeslist'])){
					$searchkey=$_POST['evntid'];
				}
				else{
					$searchkey='-';
				}

				if (isset($_POST['ascending'])) {
					$count=0;
					$conn = mysqli_connect($servername, $username, $password, $dbname);
					$read_user = "SELECT * FROM user_details ORDER BY UserFullName ASC";
					$result_read_user = mysqli_query($conn, $read_user);
					if(mysqli_num_rows($result_read_user)>0){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							$count=$count+1;
							echo "<tr>";
							echo "<td>".$count."</td>";
							echo "<td>".$row['UserID']."</td>";
							echo "<td>".$row['UserFullName']."</td>";
							echo "<td>".$row['UserEmail']."</td>";
							echo "<td>".$row['UserType']."</td>";
							echo "<tr>";
						}
					}
				}
				else if (isset($_POST['descending'])) {
					$count=0;
					$conn = mysqli_connect($servername, $username, $password, $dbname);
					$read_user = "SELECT * FROM user_details ORDER BY UserFullName DESC";
					$result_read_user = mysqli_query($conn, $read_user);
					if(mysqli_num_rows($result_read_user)>0){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							$count=$count+1;
							echo "<tr>";
							echo "<td>".$count."</td>";
							echo "<td>".$row['UserID']."</td>";
							echo "<td>".$row['UserFullName']."</td>";
							echo "<td>".$row['UserEmail']."</td>";
							echo "<td>".$row['UserType']."</td>";
							echo "<tr>";
						}
					}
				}
				else{
					$count=0;
					$conn = mysqli_connect($servername, $username, $password, $dbname);
					$read_user = "SELECT user.userNo, user.name, user.email, booking.bk_timestamp, SUM(booking.tkt_quantity) AS TICKETQUANTITY, booking.eventID FROM user INNER JOIN booking ON user.userNo = booking.userNo WHERE eventID='$searchkey%' GROUP BY user.name";	
					//$read_user = "SELECT user.name, user.email, tkt_quantity, booking.bk_timestamp FROM `user` INNER JOIN `booking` ON user.userNo=booking.userNo WHERE booking.eventID= '$searchkey%'";		
					// $read_user = "SELECT user.userNo,user.name,user.email,eventattendees.eventID,eventattendees.dateregistered,eventattendees.ticketbought FROM eventattendees INNER JOIN `user` ON eventattendees.userNo = user.userNo WHERE eventattendees.eventID = '$searchkey%'";				
					$result_read_user = mysqli_query($conn, $read_user);
					if(mysqli_num_rows($result_read_user)>0){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							$count=$count+1;
							echo "<tr>";
							echo "<td>".$count."</td>";
							echo "<td>".$row['name']."</td>";
							echo "<td>".$row['email']."</td>";
							echo "<td>".$row['bk_timestamp']."</td>";
							echo "<td>".$row['TICKETQUANTITY']."</td>";
							echo "<tr>";
						}
					}
				}
			?>
		</table>
	</div>
</body>
</html>