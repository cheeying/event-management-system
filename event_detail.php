<?php
//Connect database
include "database/connectdb.php";

//Read session
include 'session.php';

//Set Event ID
if (isset($_POST['eventID'])) {
	$eventID = $_POST['eventID'];
}

?>
<!DOCTYPE html>
<html>

<head>
	<title>UNIVERSITY Events - Event Detail/Search Event</title>
	<style>
		body {
			font-family: Arial;
			font-size: 17px;
			width: 98%;
			height: 100vh;
			/* color:#457888; */
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}

		@keyframes change {
			0% {
				background-position: 0 50%;
			}

			50% {
				background-position: 100% 50%;
			}

			100% {
				background-position: 0 50%;
			}
		}

		a:hover {
			color: lightgrey;
		}

		a {
			color: white;
			text-decoration: none;
		}

		.top {
			font-size: 34px;
			width: 80%;
			margin: auto;
			font-family: Helvetica;
			text-align: center;
			border-style: solid;
			border-width: 15px;
			border-color: white;
			color: white;
		}

		input[type=submit] {
			padding: 10px;
			color: black;
			border: none;
			background-color: #66CDAA;
			font-weight: 800;
			font-size: 14px;
			text-align: center;
			width: auto;
		}

		input[type=submit]:hover {
			background-color: #20B2AA;
		}

		form {
			margin-left: 60px;
			margin-top: 15px;
			margin-right: 60px;
		}

		table {
			margin-left: auto;
			margin-right: auto;
			width: 80%;
			padding: 20px;
			text-align: justify;
			background-color: white;
		}

		th {
			padding-bottom: 20px;
		}

		.event_name {
			border-style: none;
			font-size: 30px;
			margin-top: 10px;
		}
	</style>
</head>

<body>
	<div class="top">
		<h1>INTI EVENTS</h1>
	</div>

	<!--Display all event details area-->
	<div class="content" align="center">
		<?php
		$conn = mysqli_connect($servername, $username, $password, $dbname);

		//Read related event
		$read_DB = "SELECT * FROM event INNER JOIN venue ON event.venueID = venue.venueID  WHERE event.eventID = $eventID";

		// $read_DB = "SELECT * FROM event WHERE eventID = $eventID";
		$result = mysqli_query($conn, $read_DB);


		//Display related result and details
		if (mysqli_num_rows($result) > 0) {
			echo "<form action='payment.php' method='POST'>";
			while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
				$datetime = $row['evt_datetime'];
				$ticketsold = $row['tkt_sold'];
				$totalticket = $row['tkt_total'];
				$availableticket = $totalticket - $ticketsold;
				$date = date('Y-m-d', strtotime($datetime));
				$time = date('H:i:s', strtotime($datetime));
				echo "<table style='position:relative;'>";
				// echo "<tr><th colspan='2'><span style='border-bottom:7px solid black'><input type='hidden' name='evntID' value='" . $row['eventID'] . "'><input class ='event_name'  type='text' name='joineventname' value='" . $row['evt_name'] . "' size=80 readonly></th></tr>";
				echo "<tr><th colspan='2'><span style='border-bottom:7px solid black'><input type='hidden' id='evntID' name='evntID' value='" . $row['eventID'] . "'><input class ='event_name'  type='text' name='joineventname' value='" . $row['evt_name'] . "' size=80 readonly></th></tr>";
				echo "<tr><td rowspan='2' width='30%' style='text-align:left;vertical-align:top;padding:0'><span style='width:30px'><img src=uploads/" . $row['evt_imagename'] . " style=' display:block; width:100%; height:auto;'/></td>";
				echo "<td width='70%' style='text-align:justify;vertical-align:top;padding:0 0 0 1rem;'><span  style='font-size:16px'>" . $row['evt_description'] . "</td></tr>";
				echo "<tr>
        					<td>
        						<ul>
        							<li><b>Date: </b>" . $date . "</li>
        							<li><b>Time: </b>" . $time . "</li>
									<li><b>Price: </b>RM " . $row['tkt_price'] . "</li>
									<li><b>Ticket Available: </b>" . $availableticket . "</li>
									<li><b>Venue: </b>" . $row['v_name'] . "</li>
									<li><b>Quantity: </b>
									<input type='number' id='quantity' name='quantity' value='1' min='1' max='" . $availableticket . "' size='6'></input></li>
        							</ul></span></td></tr>";

				// echo "<tr><td style='height:50px;padding-bottom:30px;'><input type='submit' name='join' style='left:50%;position:absolute;bottom:25px' value='Join Event'/></td></tr>";
				echo "<tr><td style='height:50px;padding-bottom:30px;'><input type='submit' name='passeventid' style='left:50%;position:absolute;bottom:25px' value='Buy Ticket'/></td></tr>";
				
				echo "</table><br>";
			}
			echo "</form>";
		} else {
			echo "no data";
		}


		?>
</body>

</html>