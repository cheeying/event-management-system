<?php
 	//Connect database
	include "database/connectdb.php";
	
	// //Read session
	include 'session.php';
	// $uid=$_SESSION['UserID'];
	// if($uid=='' || $uid==null){
	// 	$message="Please login to continue";
	// 	echo "<script type='text/javascript'>alert('$message');</script>";
	// 	header("Refresh: 0, login_register.php");
	// }
?>
<!DOCTYPE html>
<html>
<style type="text/css">

body{
			width: 99%;
			height: 98%;
			/* color:#457888; */
			font-family: Arial;
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}
		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}
		a:hover {
			color: lightgrey;
			text-decoration: none;
		}

		a {
			color: white;
			text-decoration: none;
		}
			.container {
				margin-top: 20px;
			}
			.btn-primary {
				width: 100%;
			}
</style>
<div id="add">
		<form action="venue-manage.php#add" method="POST">
			<table align="center" cellspacing="20px">
				<tr><th style="text-decoration: underline;"> >>> Add New Venue <<< </th></tr>
				<tr><td><label >Name:</label> <input type="text" name="a_venuename" size="50" required></td></tr>
				<tr><td>Information:<br> <textarea type="text" name="a_venueinfo" size="30" placeholder="eg: Canteen/ For sports..." required rows="5" cols="50" required style="text-align: justify"></textarea></td></tr>
				<tr><td><input type="submit" name="addvenue" value="Add">&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="submit" name="cancel" value="Cancel"></td></tr>
			</table>
		</form>
	</div>
	<?php
		$conn = mysqli_connect($servername, $username, $password, $dbname);

		//Add venue
		if (isset($_POST['addvenue'])) {
			$vname=$_POST['a_venuename'];
			$vinfo=$_POST['a_venueinfo'];
			$insert_venue = "INSERT INTO venue (v_name, v_info) VALUES ('$vname', '$vinfo')";
			$result_insert_venue = mysqli_query($conn, $insert_venue);
			if($result_insert_venue){
    			$message="Add new venue success.";
				echo "<script type='text/javascript'>alert('$message');</script>";
			}
			else{
				$message="Fail to add new venue. Please try again.";
				echo "<script type='text/javascript'>alert('$message');</script>";
			}
		}
		?>
</body>
</html>