<?php
 	//Connect database
	include "database/connectdb.php";
	
	// //Read session
	 include 'session.php';
	// $uid=$_SESSION['UserID'];
	// if($uid=='' || $uid==null){
	// 	$message="Please login to continue";
	// 	echo "<script type='text/javascript'>alert('$message');</script>";
	// 	header("Refresh: 0, login_register.php");
	// }
?>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">

		body{
			width: 98%;
			height:98%;
			/* color:#457888; */
            font-family:Arial;
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}
		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}
        a:hover {
			color: lightgrey;
			text-decoration: none;
		}

		a {
			color: white;
			text-decoration: none;
		}
			.container {
				margin-top: 100px;
			}
			.btn-primary {
				width: 100%;
			}
</style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
            <script type="text/javascript">
                    $(document).ready(function() {					
                        $('#a_venuename').on('change',function() {
                            var sname = $(this).val();
                            var data_String = 'sname=' + sname;
                            $.get('v_search.php', data_String, function(result) {
                                $.each(result, function(){
                                    $('#a_venuename').val(this.v_name);
                                    $('#a_newvenuename').val(this.v_name);	
                                    $('#a_venueinfo').val(this.v_info);
                                });
                            });
                        });
                    });
            </script>
</head>
<div id="add">
		<form action="venue-manage-edit.php" method="POST">
			<table align="center" cellspacing="20px">
				<tr><th style="text-decoration: underline;"> >>> Edit Venue <<< </th></tr>
				<tr><td>Select venue: <select id='a_venuename' name="a_venuename">
                <?php
                   
                            $conn = mysqli_connect($servername, $username, $password, $dbname);
                            
							$read_venue = "SELECT * FROM venue";
							$result_read_venue = mysqli_query($conn, $read_venue);
							if(mysqli_num_rows($result_read_venue)>0){
								while($row = mysqli_fetch_array($result_read_venue, MYSQLI_ASSOC)){
									echo "<option value='".$row['v_name']."'>".$row['v_name']."</option>";
								}
							}
						?>
				</select>
                </td></tr>
                <tr><td>Name: <input type="text" id="a_newvenuename" name="a_newvenuename" size="30"></td></tr>
				<tr><td>Information:<br><textarea type="text" id="a_venueinfo" name="a_venueinfo" placeholder="eg: Canteen/ For sports..."  rows="5" cols="50" required style="text-align: justify"></textarea></td></tr>
				<tr><td><input type="submit" name="editvenue" value="Save">&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" name="deletevenue" value="Delete">&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="submit" name="cancel" value="Cancel"></td></tr>
			</table>
		</form>
	</div>
	<?php
		$conn = mysqli_connect($servername, $username, $password, $dbname);

		//Edit venue
		if (isset($_POST['editvenue'])) {
            $vname=$_POST['a_venuename'];
            $nvname=$_POST['a_newvenuename'];
            $vinfo=$_POST['a_venueinfo'];
            $read_venue_id="SELECT venueID from venue WHERE v_name='$vname'";
            $result_read_venue_id = mysqli_query($conn, $read_venue_id);
            if($result_read_venue_id){
                while($row = mysqli_fetch_array($result_read_venue_id, MYSQLI_ASSOC)){
                    $eid=$row['venueID'];
                    //update venue
                    $insert_venue = "UPDATE venue SET v_name='$nvname',v_info='$vinfo' WHERE venueID=$eid";
                    $result_insert_venue = mysqli_query($conn, $insert_venue);
                    if($result_insert_venue){
                        $message="Edit venue success.";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                    }
                    else{
                        $message="Fail to edit venue. Please try again.";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                    }
                }
            }
        }
        
        if (isset($_POST['deletevenue'])) {
			$selectname=$_POST['a_venuename'];
            //Read venue id
            $vname=$_POST['a_venuename'];
            $vinfo=$_POST['a_venueinfo'];
            $read_venue_id="SELECT venueID from venue WHERE v_name='$vname'";
            $result_read_venue_id = mysqli_query($conn, $read_venue_id);
            if($result_read_venue_id){
                while($row = mysqli_fetch_array($result_read_venue_id, MYSQLI_ASSOC)){
                    $eid=$row['venueID'];
                    //update venue
                    $delete_venue = "DELETE from venue WHERE venueID=$eid";
                    $result_delete_venue = mysqli_query($conn, $delete_venue);
                    if($result_delete_venue){
                        $message="Delete venue success.";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        echo "<meta http-equiv='refresh' content='0'>";

                    }
                    else{
                        $message="Fail to delete venue. Please try again.";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        echo "<meta http-equiv='refresh' content='0'>";

                    }
                }
            }
        }
        
		?>
</body>
</html>