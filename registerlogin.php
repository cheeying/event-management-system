<?php
	//Connect database
	include_once "database/connectdb.php";
		
	//To register user
	if (isset($_POST['register'])) {
		$upass = $_POST['password'];
		$upassr = $_POST['re-password'];
		$uname = $_POST['name'];
		$utype = $_POST['usertype'];
		$uemail = $_POST['email'];


		$conn = mysqli_connect($servername, $username, $password, $dbname);
		$insert_user = "INSERT INTO user (userNo, name, password, email, userType) VALUES ('null','$uname', '$upass', '$uemail', '$utype')";

		//check password reconfirmation
		if (($upass!=$upassr)){
			$message="Password and re-enter password is incorrect. Please try again.";
			echo "<script type='text/javascript'>alert('$message');</script>";
		}
		else{
			$result_insert_user = mysqli_query($conn, $insert_user);
			if($result_insert_user){
    			$message="Register success. You can login now.";
				echo "<script type='text/javascript'>alert('$message');</script>";
			}
			else{
				$message="Registration fail. Please try again.";
				echo "<script type='text/javascript'>alert('$message');</script>";
			}
		}
	}

	//To check entered details, login user, and retrieve user name and type
	if (isset($_POST['login'])) {
		$upass = $_POST['password'];
		$email = $_POST['email'];
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		$read_DB = "SELECT * FROM user WHERE email='$email' AND password='$upass'";
		$result = mysqli_query($conn, $read_DB);
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				//Compare string to check entered password and database record. Case sensitive.
				if (strcmp($upass, $row['password']) == 0) { 
					session_start();
					$_SESSION['userNo']=$row['userNo'];
					$_SESSION['name'] = $row['name'];
					$_SESSION['email'] = $row['email'];
					$_SESSION['userType'] = $row['userType'];
					$message="Login Success.";
					echo "<script type='text/javascript'>alert('$message');</script>";
					header("Refresh: 0; index.php");
					} 
				else { 
    				$message="Login Fail. Please try again.";
					echo "<script type='text/javascript'>alert('$message');</script>";
				} 
			}
		}
		else{
			$message="Login Fail. Please try again.";
			echo "<script type='text/javascript'>alert('$message');</script>";
		}
	}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Login / Register</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script>
	    $(document).ready(function(){
	        $(".btn_register").click(function(){
	          $(".btn_login").css('background-color','#ECECEC');
	          $(".btn_register").css('background-color','#D4D4D4');
	          $(".form_register").addClass("register-active");
	        });
	    });
	    $(document).ready(function(){
	        $(".btn_login").click(function(){
	          $(".btn_register").css('background-color','#ECECEC');
	          $(".btn_login").css('background-color','#D4D4D4');
	          $(".form_register").removeClass("register-active");
	        });
	    });
	    $(document).ready(function(){
	        $(".home").click(function(){
	          window.location="index.php";
	        });
	    });
    </script>
    <style>
	.btn_login{
			font-family: Times New Roman;
			color: black;
			font-size: 26px;
			font-weight: 900;
			width: 50%;
			text-align: center;
			border: none;
			background-color: #ECECEC;
			padding: 8px 0px;
			display: inline-block;
		}

		.btn_login:hover {background-color: #D4D4D4}

		.btn_login:active {
		background-color: #ECECEC;
		box-shadow: 0 5px #ECECEC;
		transform: translateY(4px);
		}
		.btn_register{
			font-family: Times New Roman;
			color: black;
			font-size: 26px;
			font-weight: 900;
			width: 50%;
			text-align: center;
			border: none;
			background-color: white;
			padding: 8px 0px;
			display: inline-block;
		}
		.btn_register:hover {background-color: #D4D4D4}

		.btn_register:active {
		background-color: #ECECEC;
		box-shadow: 0 5px #ECECEC;
		transform: translateY(4px);
		}
		input[type=submit], input[type=button]{
			padding: 10px 5px; 
			color: black;
			border: none;
			border-radius: 4px;
			background-color: #ECECEC;
			font-weight: 700;
			font-size: 18px;
			font-family: Times New Roman;
			text-align: center;
			width: 22%;
		}

		input[type=submit]:hover, input[type=button]:hover{
			background-color: #D4D4D4;
			box-shadow: 0 5px #ECECEC;
			transform: translateY(4px);
		}
		input[type=text], input[type=password], input[type=email]{
			background-color: white;
			padding: 6px 2px;
			text-align: center;
			border-style: none;
			border-bottom: 2px solid #D4D4D4;
			font-size: 18px;
			font-family: Times New Roman;
			width: 60%;
		}
	.form_login{
		margin-top: 220px;
		width: 500px;
		height: 500px;
		margin-left: 33%;
		margin-right: 33%;
		text-align: center;
		background-color: white;
		font-size: 18px;
		font-family: Times New Roman;
		z-index: 1;
		position: absolute;
	}
	.form_register{
		margin-top: 220px;
		width: 500px;
		height: 500px;
		margin-left: 33%;
		margin-right: 33%;
		text-align: center;
		background-color: white; 
		font-size: 18px;
		font-family: Times New Roman;
		position: absolute;
	}
	.register-active{
			z-index: 2;
		}
	</style>
    </head>

    <body background="src\loginbackground.jpg">
        <!--Login Form-->
        <form method="POST" class="form_login" action="registerlogin.php">
            <br><br>
            <button type="button" class="btn_login">Login</button><button type="button" class="btn_register">Register</button>
            <br><br><br>
            <span class="glyphicon glyphicon-envelope"></span><input type="text" class="" name="email" placeholder="Email" required>
		    <br><br><br>
		    <span class="glyphicon glyphicon-lock"></span><input type="password" class="" name="password" placeholder="Password" required>
		    <br><br><br>
		    <input type="submit" name="login" value="Login">&nbsp;&nbsp;&nbsp;&nbsp;
		    <input type="button" class="home" value="Home">
		    <br><br><br><br>
        </form>

        <!--Register Form-->
        <form method="POST" class="form_register" action="registerlogin.php">
		<br><br>
		<button type="button" class="btn_login">Login</button><button type="button" class="btn_register">Register</button>
        <br><br>
		<span class="glyphicon glyphicon-envelope"></span><input type="email" name="email" placeholder="E-mail" required>
        <br><br>
		<span class="glyphicon glyphicon-user"></span><input type="text" name="name" placeholder="Name" required>
		<br><br>
		<span class="glyphicon glyphicon-lock"></span><input type="password" name="password" placeholder="Password" required>
		<br><br>
		<span class="glyphicon glyphicon-lock"></span><input type="password" name="re-password" placeholder="Re-enter Password" required>
		<br><br>
		<span class="glyphicon glyphicon-pawn"></span><input type="text" name="usertype" value="member" placeholder="User Type" readonly>
		<br><br>
		<input type="submit" name="register" value="Register">&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" class="home" value="Home">
	</form>
    </body>
</html>