<?php
	//Connect database
	include "database/connectdb.php";

	//Read session
    include 'session.php';
	
	
	$name=$_SESSION['name'];
	if($name=='' || $name==null){
        $message="Please login to continue";
		echo "<script type='text/javascript'>alert('$message');</script>";
		header("Refresh: 0, registerlogin.php");
	}
	else{
		$userNo=$_SESSION['userNo'];
	}
?>

<!DOCTYPE html>
<html>
<head>	
	<title>My Bookings</title>
	<style type="text/css">
		body{
			font-family: Arial;
  			font-size: 17px;
			width: 98%;
			height:100vh;
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}

		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}
        a:hover{
			color:lightgrey;
		}
		a{
			color:  white;
			text-decoration: none;
		}
        .top{
			font-size: 34px;
			font-family: Helvetica;
			text-align: center;
            border-style: solid;
            border-width: 15px;
            border-color: white; 
            color: white;
            background-color: transparent;
		}
		form{
			margin-left: 60px;
			margin-top: 15px;
			margin-right: 60px;
		}
		table{
			max-width: 1200px;
            width: 100%;
			margin-bottom:20px;
			margin-left:auto;
			margin-right:auto;
			text-align:center;
			padding-top: 10px;
            padding-bottom: 10px;
			padding-left: 20px;
			padding-right: 20px;
		}
		th{
			background-color: #EFDEF0;
			font-size: 20px;
			text-align: center;
			padding: 5px 10px;
		}
		td{
			font-size: 18px;
			padding: 5px 5px;
            background-color: white;
		}

		.evtname_style{
			color:  black;
		}
		div{
			margin: auto;
			padding-bottom: 5px;
			min-width: 50%;
			max-width: 80%;
		}
		hr{
			border-top: 5px dotted grey;
			border-bottom: none;
			border-left: none;
			border-right: none;
			width: 95%;
			padding-top: 10px
		}
	</style>
</head>
<body>
    <div class="top">
            <h1>My Booking</h1>
    </div>


	<div id="view" align="center">
        <br>
        
        <table cellpadding="15px" cellspacing="7px" align="center">
            <tr><th colspan="4" style="font-size: 30px; font-weight: 1000; background-color:#B4C6DD;">My Profile</th></tr>
            <tr>
				<th style="background-color:#B4C6DD;">User No</th>
				<th style="background-color:#B4C6DD;">Name</th>
				<th style="background-color:#B4C6DD;">E-mail</th>
				<th style="background-color:#B4C6DD;">User-Type</th>
			</tr>
			<tr>
				<?php
					$read_user = "SELECT userNo FROM user WHERE name='$name'";
					$result_read_user = mysqli_query($conn, $read_user);
					if($result_read_user){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							// echo "<th style='text-align:right;' width='16%'><b>User No: </b></th>";
							echo "<td>".$row['userNo']."</td>";
						}
					}
				?>
				<?php
					$read_user = "SELECT name FROM user WHERE name='$name'";
					$result_read_user = mysqli_query($conn, $read_user);
					if($result_read_user){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							// echo "<th style='text-align:right;' width='16%'><b>Name: </b></th>";
							echo "<td>".$row['name']."</td>";
						}
					}
				?>
				<?php
					$read_user = "SELECT email FROM user WHERE name='$name'";
					$result_read_user = mysqli_query($conn, $read_user);
					if($result_read_user){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							// echo "<th style='text-align:right;' width='16%'><b>E-mail: </b></th>";
							echo "<td>".$row['email']."</td>";
						}
					}
				?>
				<?php
					$read_user = "SELECT userType FROM user WHERE name='$name'";
					$result_read_user = mysqli_query($conn, $read_user);
					if($result_read_user){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							// echo "<th style='text-align:right;' width='16%'><b>User Type: </b></th>";
							echo "<td>".$row['userType']."</td>";
						}
					}
				?>
			</tr>
		</table>
		<form id="goEvent" class="hidden" action='event_detail.php' method='POST'>
			<input id="eventID" type='hidden' name='eventID' value=''>
		</form>
		
		<table align="center" cellpadding="15px" cellspacing="7px">
        <tr><th colspan = 6 style="font-size: 30px; font-weight: 1000;">My Booking</th></tr>
        <br>
			<tr>
				<th>No.</th>
				<th>Booking<br>Date & Time</th>
				<th>Event Name</th>
				<th>No. of ticket <br>purchased</th>
				<th>Event<br>Date & Time</th>
				<th>Venue</th>
			</tr>
			<!--Get all booking record of the user-->
			<?php
				
				$count=0;
				$conn = mysqli_connect($servername, $username, $password, $dbname);
				//Read user booking detail
				$read_user_booking = "SELECT * FROM booking WHERE userNo='$userNo'";
				$result_read_user_booking = mysqli_query($conn, $read_user_booking);
				if ($result_read_user_booking){
					while($row = mysqli_fetch_array($result_read_user_booking, MYSQLI_ASSOC)){
						$eid=$row['eventID'];
						$count=$count+1;
						echo "<tr>";
						echo "<td>".$count."</td>";
						echo "<td>".$row['bk_timestamp']."</td>";
						//Read event detail
						$read_event_detail = "SELECT * FROM event WHERE eventID='$eid'";
						$result_read_event_detail = mysqli_query($conn, $read_event_detail);
						if ($result_read_event_detail){
							while($row1 = mysqli_fetch_array($result_read_event_detail, MYSQLI_ASSOC)){
								$vid=$row1['venueID'];
								echo "<td> <a href='javascript:void(0)' class='evtname_style' onclick='eventIsClick(".$row1['eventID'].")'>".$row1['evt_name']."</a></td>";
								echo "<td>".$row['tkt_quantity']."</td>";
								echo "<td>".$row1['evt_datetime']."</td>";
								//Read venue detail
								$read_venue_detail = "SELECT * FROM venue WHERE venueID='$vid'";
								$result_read_venue_detail = mysqli_query($conn, $read_venue_detail);
								if ($result_read_event_detail){
									while($row2 = mysqli_fetch_array($result_read_venue_detail, MYSQLI_ASSOC)){
										echo "<td>".$row2['v_name']."</td>";
									}
								}
							}
						}
						echo "</tr>";
					}
				}
			?>
		</table>
	</div>
	<script>
		function eventIsClick(eventID){
			document.getElementById('eventID').value = eventID;
			document.getElementById('goEvent').submit();
		}
	</script>
</body>
</html>