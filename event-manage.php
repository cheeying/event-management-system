<?php
	//Connect database
	include "database/connectdb.php";

	// //Read session
	include 'session.php';
	// $uid=$_SESSION['UserID'];
	// if($uid=='' || $uid==null){
	// 	$message="Please login to continue";
	// 	echo "<script type='text/javascript'>alert('$message');</script>";
	// 	header("Refresh: 0, login_register.php");
	// }

	// //Read button script
	// include "top_button.html";

?>

<!DOCTYPE html>
<html>
	<head>
        <title>All Events - Add Event</title>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
		<style type="text/css">
		body{
			width: 99%;
			height: 98%;
			/* color:#457888; */
			/*PUSH*/
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}
		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}
		a:hover {
			color: lightgrey;
			text-decoration: none;
		}

		a {
			color: white;
			text-decoration: none;
		}
			.container {
				margin-top: 100px;
			}
			.btn-primary {
				width: 100%;
			}
		</style>
		

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

		<script type='text/javascript'>
			$( document ).ready(function() {
				var date = new Date();
            	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
				$('#datetimepicker1').datetimepicker({
					// date_format: 'yyyy-mm-dd hh:ii',
					format: 'YYYY-MM-DD HH:mm',
					minDate: today
				});
			});
		</script>

		

	</head>

	<div class="wrapper">
		<div class="row">
			<div class="col-md-10 col-md-push-1 col-lg-6 col-lg-push-3">
				<div class="box box-default" align="center">
					<div class="box-body">

						<div id="add">
							<form action="event-manage.php#add" method="POST" enctype="multipart/form-data">
								<fieldset>
									<legend>
									Add New Event
									</legend>
									<div class="card-content">
										<table>
											<thead>
												<tr>
													<th>Event Title:</th>
													<th><input type="text" name="a_eventname" size="31" required></input></th>
												</tr>
											</thead>
										
											<tbody>
												<tr>
													<td><label style='margin-top: 20%'>Upload image:</label></td>
													<td><input id= "fileToUpload" class="d-block" type="file" name="fileToUpload" size="35" style="margin-top: 7%" required></input></td>
												</tr>
												<tr>
													<td><label style='margin-top: 20%'>Event Date&Time:</label></td>
													<td><div class='input-group date' id='datetimepicker1' style="margin-top: 7%" required>
													<input id = 'a_datetime'name ='a_datetime' type='text' class="form-control" size="27" />
													<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
													</span>
													</div>
													</td>
												</tr>
												<tr>
													<td><label style='margin-top: 20%'>Event Category:</label></td>
													<td><textarea name="a_eventcategory" rows="2" cols="50" placeholder="eg: Concert, Sports, Talk, Festival etc..." required  style="margin-top: 7%"></textarea>
													</td>
												</tr>
												<tr>
													<td><label style='margin-top: 20%'>Event Description:</label></td>
													<td><textarea name="a_eventdescription" rows="5" cols="50" required style="text-align: justify; margin-top: 7%"  ></textarea>
													</td>
												</tr>
												<tr>
													<style type="text/css">     
														select {
															width:280px;
														}
													</style>
													<td><label style='margin-top: 20%'>Venue:</label></td>
													<td><select name="a_eventvenue"  style="margin-top: 7%" >
													<?php
														$conn = mysqli_connect($servername, $username, $password, $dbname);
														$read_venue = "SELECT * FROM venue";
														$result_read_venue = mysqli_query($conn, $read_venue);
														if(mysqli_num_rows($result_read_venue)>0){
															while($row = mysqli_fetch_array($result_read_venue, MYSQLI_ASSOC)){
																echo "<option value='".$row['v_name']."'>".$row['v_name']."</option>";
															}
														}
													?>
													</select></td>
												</tr>
												<tr>
													<td><label style='margin-top: 20%'>Ticket Price: RM </label></td>
													<td><input type="number" name="a_eventticketprice" min="00" placeholder="0" size="35" required  style="margin-top: 7%">.00 </td>
												</tr>
												<tr>
													<td><label style='margin-top: 20%'>Number of Ticket: </label></td>
													<td><input type="number" name="a_eventtickettotal" min="10" placeholder="10" size="35" required  style="margin-top: 7%"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</fieldset>
								<div class="text-center">
									<!-- <button type="submit" id="item_save" name="save" class="btn btn-success"><i class="fa fa-check"></i>&nbsp; Save</button> -->
									<!-- <button type="submit" id="item_saveAndAdd" name="item[saveAndAdd]" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp; Save &amp; Add Another</button> -->
									<tr><td colspan="2"><input type="submit" name="addevent" value="Add"  style="margin-top: 4%">&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="reset" name="cancel" value="Cancel"  style="margin-top: 4%"></td></tr>
								</div>

								<!-- <table align="center" cellspacing="20px">
									<tr><th style="text-decoration: underline;"> >>> Add New Event <<< </th></tr>
									<tr><td>Event Title: <br><input type="text" name="a_eventname" size="35" required></td></tr>
									<tr><td>Upload image:
										<input id= "fileToUpload" class="d-block" type="file" name="fileToUpload" size="20"></input>
									</td></tr>
									<tr>
										<td>Event Date&Time:
										<div class='input-group date' id='datetimepicker1'>
										<input id = 'a_datetime'name ='a_datetime' type='text' class="form-control" />
										<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
										</span>
										</div>
										</td>
									</tr>
									<tr><td>Event Category: <br><textarea name="a_eventcategory" rows="2" cols="50" placeholder="eg: Concert, Sports, Talk, Festival etc..." required></textarea></td></tr>
									<tr><td>Event Description: <br><textarea name="a_eventdescription" rows="5" cols="50" required style="text-align: justify"></textarea></td></tr>
									<tr><td>Venue:
										<select name="a_eventvenue" >
											
										</select>
									</td></tr>
									<tr><td>Ticket Price: RM <input type="number" name="a_eventticketprice" min="00" placeholder="0" required>.00 </td></tr>
									<tr><td>Number of Ticket: <input type="number" name="a_eventtickettotal" min="10" placeholder="10" required></td></tr>
									<tr><td colspan="2"><input type="submit" name="addevent" value="Add">&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="reset" name="cancel" value="Cancel"></td></tr>
								</table> -->
							</form>
						</div>
				    </div>
				</div>
			</div>
		</div>

	<?php
		$conn = mysqli_connect($servername, $username, $password, $dbname);

		if (isset($_POST['addevent'])) {
			$ename=$_POST['a_eventname'];
			$gettime=$_POST['a_datetime'];
			$converttime=strtotime($gettime);
			$timeformat=date("Y-m-d H:i:s",$converttime);
			$edatetime=$timeformat;
			$edescription=$_POST['a_eventdescription'];
			$ecategory=$_POST['a_eventcategory'];
			$evenue=$_POST['a_eventvenue'];
			$eprice=$_POST['a_eventticketprice'];
			$etotal=$_POST['a_eventtickettotal'];
			$found=false;

			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$name =addslashes($_FILES['fileToUpload']['name']);
			$image=addslashes($_FILES['fileToUpload']['tmp_name']);
			if($image!=""){
				$image= file_get_contents($image);
				$image= base64_encode($image);
			}

			// $uploadOk = 1;
			// $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// move_uploaded_file($name,$target_dir.'/'.$target_file);

			// // Check if image file is a actual image or fake image
			// if(isset($_POST["submit"])) {
			// 	$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			// 	if($check !== false) {
			// 	echo "File is an image - " . $check["mime"] . ".";
			// 	$uploadOk = 1;
			// 	} else {
			// 	echo "File is not an image.";
			// 	$uploadOk = 0;
			// 	}
			// }

			// // Check file size
			// if ($_FILES["fileToUpload"]["size"] > 500000) {
			// 	echo "Sorry, your file is too large.";
			// 	$uploadOk = 0;
			// }

			// // Allow certain file formats
			// if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			// && $imageFileType != "gif" ) {
			// echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			// $uploadOk = 0;
			// }
			$read_event_id = "SELECT * FROM event";
			$result_read_event_id = mysqli_query($conn, $read_event_id);
			while($row = mysqli_fetch_array($result_read_event_id)){
				$resultset[] = $row;
			}
			foreach($resultset as $result){
				if($ename==$result['evt_name']){
					$message = 'Event name already exist.';
					$found=true;
					echo "<script type='text/javascript'>alert('$message');</script>";
					echo "<meta http-equiv='refresh' content='0'>";  
				break;
				}
			}
			if($found!=true){
				//Read venue id
			$read_venue_id="SELECT venueID FROM venue WHERE v_name='$evenue'";
			$result_read_venue_id = mysqli_query($conn, $read_venue_id);

				if($result_read_venue_id){
					while($row = mysqli_fetch_array($result_read_venue_id, MYSQLI_ASSOC)){
						//Upload image
						move_uploaded_file($_FILES['fileToUpload']['tmp_name'],"uploads/$name");

						$vid=$row['venueID'];
						//Insert event
						$insert_event = "INSERT INTO event (evt_name, evt_datetime,evt_cat, evt_description, tkt_price, tkt_total,evt_image ,evt_imagename, venue_name, venueID) VALUES ('$ename', '$edatetime', '$ecategory', '$edescription', $eprice, $etotal,'$image','$name','$evenue', $vid)";
						$result_insert_event = mysqli_query($conn, $insert_event);
						if($result_insert_event){
							$message="Add new event success.";
							echo "<script type='text/javascript'>alert('$message');</script>";
						}
						else{
							$message="Fail to add new event. Please try again.";
							echo "<script type='text/javascript'>alert('$message');</script>";
						}
					}
				}
			}
		}
		?>
</html>
