<?php
    //Connect database
	include "database/connectdb.php";
	
	//Read session
    include 'session.php';
    
    if (isset($_POST['adduser'])) {
        header('Refresh: 0; user-manage.php');
    }
    else if (isset($_POST['edituser'])) {
        header('Refresh: 0; members-list-view.php');
    }
    else if (isset($_POST['userlist'])) {
        header('Refresh: 0; members-list-view.php');
    }
    //Event Management
    else if (isset($_POST['addevent'])) {
        header('Refresh: 0; event-manage.php');
    }
    else if (isset($_POST['editevent'])) {
        header('Refresh: 0; event-manage-edit.php');
    }
    else if (isset($_POST['eventlist'])) {
        header('Refresh: 0; event-list-view.php');
    }
    //Venue Management
    else if (isset($_POST['addvenue'])) {
        header('Refresh: 0; venue-manage.php');
    }
    else if (isset($_POST['editvenue'])) {
        header('Refresh: 0; venue-manage-edit.php');
    }
    else if (isset($_POST['venuelist'])) {
        header('Refresh: 0; venue-list-view.php');
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel</title>
        <style type="text/css">
            .boxposition{
                margin-top: 220px;
                width: 550px;
                height: 400px;
                margin-left: 35%;
                margin-right: 33%;
                text-align: center;
                background-color: white;
                font-size: 18px;
                font-family: Times New Roman;
                z-index: 1;
                position: absolute;
            }
            input[type=submit], input[type=button]{
			padding: 10px 5px; 
			color: black;
			border: none;
			border-radius: 4px;
			background-color: #ECECEC;
			font-weight: 700;
			font-size: 18px;
			font-family: Times New Roman;
			text-align: center;
			width: 100%;
            }

            input[type=submit]:hover, input[type=button]:hover{
                background-color: #D4D4D4;
                box-shadow: 0 5px #ECECEC;
                transform: translateY(4px);
            }
        </style>
    </head>
    <body background="src\loginbackground.jpg">
        <form method="POST" class="boxposition" action="">
            <p style="font-size:30px">ADMIN MANAGEMENT</p>
            <table cellspacing="20px" align="center">
                <tr>
                    <td><p style="font-size:24px">User &nbsp;&nbsp;: </td>
                    <!-- <td><input type="submit" name="adduser" value="Add"></td> -->
                    <!-- <td><input type="submit" name="edituser" value="Edit"></td> -->
                    <td><input type="submit" name="userlist" value="List"></td>
                </tr>
                <tr>
                    <td><p style="font-size:24px">Event&nbsp;: </td>
                    <td><input type="submit" name="addevent" value="Add"></td>
                    <td><input type="submit" name="editevent" value="Edit"></td>
                    <td><input type="submit" name="eventlist" value="List"></td>
                </tr>
                    <td><p style="font-size:24px">Venue&nbsp;: </td>
                    <td><input type="submit" name="addvenue" value="Add"></td>
                    <td><input type="submit" name="editvenue" value="Edit"></td>
                    <td><input type="submit" name="venuelist" value="List"></td>
                <tr>
                </tr>
            </table>
        </form>
    </body>
</html>