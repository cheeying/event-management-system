<?php
session_start();
class UnitTest  extends \PHPUnit\Framework\TestCase{
    
    /** @test */
    public function check_assert_true()
    {
        $this->assertTrue(true);
    }

    /** @testhghg */
    public function make_sure_db_name_is_correct()
    {
        $this->assertEquals($GLOBALS['dbname'],'etms');
    }

    /** @test */
    public function check_db_credentials_is_correct()
    {
        // $this->assertEquals($GLOBALS['username'],'root');
        $this->assertTrue(($GLOBALS['password']==false && $GLOBALS['username'] == 'root'));
    }

    /** @test */
    public function check_db_is_connected()
    {
        $conn = mysqli_connect($GLOBALS['servername'], $GLOBALS['username'], $GLOBALS['password'], $GLOBALS['dbname']);
        $this->assertTrue(mysqli_ping($conn) ? true : false);
    }

    /** @test */
    public function check_login()
    {
        $conn = mysqli_connect($GLOBALS['servername'], $GLOBALS['username'], $GLOBALS['password'], $GLOBALS['dbname']);
        $is_login = false;
        $upass = 'sm12';
        $email ='sm@hotmail.com';
        $read_DB = "SELECT * FROM user WHERE email='$email' AND password='$upass'";
		$result = mysqli_query($conn, $read_DB);
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				//Compare string to check entered password and database record. Case sensitive.
				if (strcmp($upass, $row['password']) == 0) { 
					$_SESSION['name'] = $row['name'];
					$_SESSION['email'] = $row['email'];
                    $_SESSION['userType'] = $row['userType'];
                    $is_login = true;
				} 
			}
		}
        $this->assertTrue($is_login,$this->printError("Credentials do not match with records"));
    }

    /** @test */
    public function check_can_purchase_if_ticket_soldout()
    {
        $auth = isset($_SESSION['name']);
        $this->assertTrue($auth,$this->printError("Login is required for purchasing ticket"));
    }

    /** @test */
    // public function check_can_purchase_before_login()
    // {
        
    // }

    // /** @test */
    // public function check_can_purchase()
    // {

    // }

    // /** @test */
    // public function check_can_view_event_detail()
    // {

    // }

    /** @test */
    public function accurate_event_detail()
    {
        $conn = mysqli_connect($GLOBALS['servername'], $GLOBALS['username'], $GLOBALS['password'], $GLOBALS['dbname']);
        $read_DB = "SELECT * FROM event WHERE eventID=1";
        $result = mysqli_query($conn, $read_DB);
        $row = $result->fetch_assoc();
        $data_is_correct = true;
        if($row){
            if($row['evt_name'] != "Fun Fair"){
                $data_is_correct = false;
            }
        }
        $this->assertTrue($data_is_correct,$this->printError("Event data input is incorrect!"));
    }

    // /** @test */
    // public function check_can_view_booking_detail()
    // {

    // }

    // /** @test */
    // public function check_can_view_booked_event_detail()
    // {

    // }

    // /** @test */
    // public function check_can_book_multiple_times()
    // {

    // }

    /**
     * @group
     */
    protected function printError($msg="")
    {
        return "******************************\n***********ERROR*************\n*****************************\n".$msg."\n*****************************\n";
    }
}