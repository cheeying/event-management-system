<?php
//Connect database
include "database/connectdb.php";

//Read session
include 'session.php';

//Set EventId and Quantity
if (isset($_POST['passeventid'])) {
  //Go login page if not login
  if ($login_status == "no") {
		$message = "Please login to continue.";
		echo "<script type='text/javascript'>alert('$message');</script>";
		header("Refresh: 0; registerlogin.php");
	}
  $eventID = $_POST['evntID'];
  $quantity = $_POST['quantity'];
}


if (isset($_POST['checkout'])) {
  $fname = $_POST['firstname'];
  $email = $_POST['email'];
  $adr = $_POST['address'];
  $city = $_POST['city'];
  $state = $_POST['state'];
  $zip = $_POST['zip'];
  $eventID1 = $_POST['evntID1'];
  $ttlammount = $_POST['ttl1'];
  $quantity1 = $_POST['qty1'];
  //update tkt total
  $conn = mysqli_connect($servername, $username, $password, $dbname);
  $read_ticket_info = "SELECT tkt_sold from event WHERE eventID='$eventID1'";
  $result_read_ticket_info = mysqli_query($conn, $read_ticket_info);
  if (mysqli_num_rows($result_read_ticket_info) > 0) {
    echo "<form action='join_event.php' method='POST'>";
    while ($row = mysqli_fetch_array($result_read_ticket_info, MYSQLI_ASSOC)) {
      $ticketsold = $row['tkt_sold'];
    }
  }
  $currentsoldticket=$ticketsold+$quantity1;
  $update_ticket_sold = "UPDATE event SET tkt_sold=$currentsoldticket WHERE eventID='$eventID1'";
  $result_update_ticket_sold = mysqli_query($conn, $update_ticket_sold);

  //insert into booking
  date_default_timezone_set('Asia/Kuala_Lumpur');
  $current_time = date('Y-m-d H:i:s');
  $insert_booking = "INSERT INTO booking (bk_timestamp, userNo, eventID, tkt_quantity) VALUES ('$current_time', '$uNo', '$eventID1','$quantity1' ) ";
  $result_insert_booking = mysqli_query($conn, $insert_booking);



  $insert_payment = "INSERT INTO payment (fullname,email,address,city,state,zip,amount) VALUES ('$fname','$email','$adr','$city','$state','$zip','$ttlammount')";
  $result_insert_payment = mysqli_query($conn, $insert_payment);
  if ($result_insert_payment) {
    $message = "Transaction successful. Thank you.";
    echo "<script type='text/javascript'>
          alert('$message')
          window.location.replace('http://localhost/event-management-system/booking.php');</script>";
  } else {
    $message = "Transaction fail. Please try again.";
    echo "<script type='text/javascript'>alert('$message');</script>";
  }
}

?>

<!DOCTYPE html>
<html>

<head>
  <title>Payment</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    body {
      font-family: Arial;
      font-size: 17px;
      padding: 8px;
      background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
      background-size: 400% 400%;
      position: relative;
      animation: change 10s ease-in-out infinite;
    }

    * {
      box-sizing: border-box;
    }

    .row {
      display: -ms-flexbox;
      /* IE10 */
      display: flex;
      -ms-flex-wrap: wrap;
      /* IE10 */
      flex-wrap: wrap;
      margin: 0 -16px;
    }

    .col-25 {
      -ms-flex: 25%;
      /* IE10 */
      flex: 25%;
    }

    .col-50 {
      -ms-flex: 50%;
      /* IE10 */
      flex: 50%;
    }

    .col-75 {
      -ms-flex: 75%;
      /* IE10 */
      flex: 75%;
    }

    .col-25,
    .col-50,
    .col-75 {
      padding: 0 16px;
    }

    .container {
      background-color: #f2f2f2;
      padding: 5px 20px 15px 20px;
      border: 1px solid lightgrey;
      border-radius: 3px;
    }

    input[type=text] {
      width: 100%;
      margin-bottom: 20px;
      padding: 12px;
      border: 1px solid #ccc;
      border-radius: 3px;
    }

    label {
      margin-bottom: 10px;
      display: block;
    }

    .icon-container {
      margin-bottom: 20px;
      padding: 7px 0;
      font-size: 24px;
    }

    .btn {
      background-color: #4CAF50;
      color: white;
      padding: 12px;
      margin: 10px 0;
      border: none;
      width: 100%;
      border-radius: 3px;
      cursor: pointer;
      font-size: 17px;
    }

    .btn:hover {
      background-color: #45a049;
    }

    @keyframes change {
      0% {
        background-position: 0 50%;
      }

      50% {
        background-position: 100% 50%;
      }

      100% {
        background-position: 0 50%;
      }
    }

    a:hover{
			color:lightgrey;
		}
		a{
			color:  white;
			text-decoration: none;
		}

    hr {
      border: 1px solid lightgrey;
    }

    span.price {
      float: right;
      color: grey;
    }

    /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
    @media (max-width: 800px) {
      .row {
        flex-direction: column-reverse;
      }

      .col-25 {
        margin-bottom: 20px;
      }
    }
  </style>
</head>

<body>


  <div class="row">
    <div class="col-75">
      <div class="container">
        <form action="payment.php" method="POST">
          <div class="row">
            <div class="col-50">
              <h3>Billing Address</h3>
              <label for="fname"><i class="fa fa-user"></i> Full Name</label>
              <input type="text" id="fname" name="firstname" placeholder="Enter name..." required="required">
              <label for="email"><i class="fa fa-envelope"></i> Email</label>
              <input type="text" id="email" name="email" placeholder="john@example.com" required="required">
              <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
              <input type="text" id="adr" name="address" placeholder="Enter address..." required="required">
              <label for="city"><i class="fa fa-institution"></i> City</label>
              <input type="text" id="city" name="city" placeholder="Enter city..." required="required">

              <div class="row">
                <div class="col-50">
                  <label for="state">State</label>
                  <input type="text" id="state" name="state" placeholder="Enter state..." required="required">
                </div>
                <div class="col-50">
                  <label for="zip">Zip</label>
                  <input type="text" id="zip" name="zip" placeholder="Enter zip..." required="required">
                </div>
              </div>
            </div>

            <div class="col-50">
              <h3>Payment</h3>
              <label for="fname">Accepted Cards</label>
              <div class="icon-container">
                <i class="fa fa-cc-visa" style="color:navy;"></i>
                <i class="fa fa-cc-amex" style="color:blue;"></i>
                <i class="fa fa-cc-mastercard" style="color:red;"></i>
                <i class="fa fa-cc-discover" style="color:orange;"></i>
              </div>
              <label for="cname">Name on Card</label>
              <input type="text" id="cname" name="cardname" placeholder="Enter cardholder name..." required="required">
              <label for="ccnum">Credit card number</label>
              <input type="text" id="ccnum" name="cardnumber" placeholder="Enter card number..." required="required">
              <label for="expmonth">Exp Month</label>
              <input type="text" id="expmonth" name="expmonth" placeholder="Enter expire month..." required="required">
              <div class="row">
                <div class="col-50">
                  <label for="expyear">Exp Year</label>
                  <input type="text" id="expyear" name="expyear" placeholder="Enter expire year..." required="required">
                </div>
                <div class="col-50">
                  <label for="cvv">CVV</label>
                  <input type="text" id="cvv" name="cvv" placeholder="Enter CVV..." required="required">
                </div>
              </div>
            </div>
          </div>
          <input type="submit" name="checkout" value="Pay" class="btn">
        
      </div>
    </div>
        <?php
        $conn = mysqli_connect($servername, $username, $password, $dbname);
        //Read related event
        $read_DB = "SELECT * FROM event INNER JOIN venue ON event.venueID = venue.venueID  WHERE event.eventID = " . $eventID . "";
        // $read_DB = "SELECT * FROM event WHERE eventID = $eventID";
        $result = mysqli_query($conn, $read_DB);
        //Display related result and details
        if (mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $ticketprice = $row['tkt_price'];
            $total = $ticketprice * $quantity;
            echo "<div class='col-25'>";
            echo "<div class='container'>";
            echo "<h4>Purchase Summary </h4>";
            echo "<input type='hidden' id='evntID1' name='evntID1' value='" . $eventID . "'>";
            echo "<input type='hidden' id='ttl1' name='ttl1' value='" . $total . "'>";
            echo "<input type='hidden' id='qty1' name='qty1' value='" . $quantity . "'>";
            echo "<p>Event Name<span class='price'>" . $row['evt_name'] . "</span></p>";
            echo "<p>Price <span class='price'>" . $row['tkt_price'] . "</span></p>";
            echo "<p>Quantity <span class='price'>" . $quantity . "</span></p><hr>";
            echo "<p>Total <span class='price' style='color:black'><b>" . $total . "</b></span></p>";
            echo "</div>";
            echo "</div>";
          }
        }
        ?>
    </form> 
</body>
</html>