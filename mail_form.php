<?php
	//Connect database
    include "database/connectdb.php";
    include 'session.php';
    include 'email.php';
	// //Read session
	// include 'session.php';
	// $uid=$_SESSION['UserID'];
	// if($uid=='' || $uid==null){
	// 	$message="Please login to continue";
	// 	echo "<script type='text/javascript'>alert('$message');</script>";
	// 	header("Refresh: 0, login_register.php");
	// }

	// //Read button script
	// include "top_button.html";
	
?>  
<!DOCTYPE html>
<html>
<head>
    <title>Send an Email</title>
    	<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>

        <!-- Script -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src='select2/dist/js/select2.min.js' type='text/javascript'></script>

        <!-- CSS -->
        <link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>

		<style type="text/css">
		body{
			width: 99%;
			height:98%;
			/*PUSH9*/
			/* color:#457888; */
            font-family: Arial;
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}
        select{
            width: 250px;
            margin: 10px;
        }  
        select:focus {
            min-width: 250px;
            width: auto;
        }
		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}
        a:hover {
			color: lightgrey;
			text-decoration: none;
		}

		a {
			color: white;
			text-decoration: none;
		}
			.container {
				margin-top: 100px;
			}
			.btn-primary {
				width: 100%;
			}
		</style>
        <script type="text/javascript">
		
        $(document).ready(function() {
            // $('#datetimepicker1').datetimepicker();
            $("#a_eventname").select2();

            // $('#a_eventname').on('change',function() {
            //     var sname = $(this).val();
            //     var data_String = 'sname=' + sname;
            //     var date = new Date();
            //     var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            //     $.get('search.php', data_String, function(result) {
            //         $.each(result, function(){
            //             $('#a_eventname').val(this.evt_name);
            //             $('#picture').attr("src",'uploads/'+this.evt_imagename);
            //             $('#a_datetime').val(this.evt_datetime);	
            //             $('#a_eventcategory').val(this.evt_cat);
            //             $('#a_eventdescription').val(this.evt_desrciption);
            //             $('#a_eventvenue').val(this.venue_name);
            //             $('#a_eventticketprice').val(this.tkt_price);
            //             $('#a_eventtickettotal').val(this.tkt_total);
                        
            //         });			
            //     });
            // });
            
        });

        



</script>
		
</head>
<body>
		<h4 class="sent-notification"></h4>
        <div class="wrapper">
		<div class="row">
			<div class="col-md-10 col-md-push-1 col-lg-6 col-lg-push-3">
				<div class="box box-default" align="center">
					<div class="box-body">
						<div id="edit">
                            <form id="myForm" method="POST" action="mail_form.php">
                                <fieldset>
                                    <legend align="Center">
									>>> Send Notification <<<
									</legend>
                                
                                <div class="card-content">
                                    <table>
                                        <thead>
                                            <tr>
                                            <td><label>Event Title :</label></td>
                                                <td><select name="a_eventname" id="a_eventname" width="100px">
                                                    <option value='0'>--Select Event--</option> 
                                                    <?php
                                                        $conn = mysqli_connect($servername, $username, $password, $dbname);
                                                        $read_venue = "SELECT * FROM event";
                                                        $result_read_venue = mysqli_query($conn, $read_venue);
                                                        if(mysqli_num_rows($result_read_venue)>0){
                                                            while($row = mysqli_fetch_array($result_read_venue, MYSQLI_ASSOC)){
                                                                echo "<option value='".$row['evt_name']."'>".$row['evt_name']."</option>";
                                                            }
                                                        }
                                                    ?>
                                                </select></td>
                                            </tr>
                                        </thead>
                                        </td></tr>
                                        <tbody>
                                            <tr>
                                                <td><label>Subject :</label></td>
                                                <td><input id="subject" name="subject"  type="text" size="30" placeholder=" Enter Subject"  required></td> 
                                            </tr>
                                            <tr>
                                                <td><label>Message :</label></td>
                                                <td><textarea id="body" name="body" rows="5" cols="35" placeholder="Type Message" required></textarea></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                <div>
                                <td><input type="submit" name="sendmail" value="Send">&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="reset" name="cancel" value="Cancel"  style="margin-right :65px"></td>

                                </fieldset>
                                <!-- <label>Email :</label>
                                <input id="email" type="text" placeholder="Enter Email">
                                <br><br> -->

                                <!-- <button type="button" onclick="sendEmails()" value="Send An Email">Submit</button>  -->

                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

		

    <?php
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		if (isset($_POST['sendmail'])) {
           
			$event=$_POST['a_eventname'];
			$subject=$_POST['subject'];
			$body=$_POST['body'];
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $currentDateTime = date('Y-m-d H:i:s');
			//Read venue id
            //$read_venue_id="SELECT venueID FROM venue WHERE v_name='$evenue'";
            if($event!='0'){
                $read_event_id="SELECT * FROM event WHERE evt_name='$event'";
                $result_read_event_id=mysqli_query($conn, $read_event_id);
                //$result_read_venue_id = mysqli_query($conn, $read_venue_id);
                if($result_read_event_id){
                   // if($result_read_venue_id){
                        while($row=mysqli_fetch_array($result_read_event_id, MYSQLI_ASSOC)){
                            emailAll($row['eventID'],$subject,$body);
                            $insert_noti="INSERT into notification (eventID, message, datetime) VALUES ('$row[eventID]','$body', '$currentDateTime')";
                            $result_insert_noti = mysqli_query($conn, $insert_noti);
                            if($result_insert_noti){
                                $message="Email has sent to all participant";
                                echo "<script type='text/javascript'>alert('$message');</script>";
                            }
                            }
                        }
                    //}
                }
            //}
            else{
                $message="Please Select an Event";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }
        }
        
        ?>
	

</body>
</html>
                           