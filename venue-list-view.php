<?php
    include_once "database/connectdb.php";
    include 'session.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>ETMS - Venue List</title>

	<style type="text/css">

		.event_title{
				font-weight: 900;
				font-size: 55px;
				font-family: Helvetica;
				text-align: center;
				color: white;
		}
		a:hover {
			color: lightgrey;
		}

		a {
			color: white;
			text-decoration: none;
		}
		form{
			margin-left: 60px;
			margin-top: 40px;
			margin-right: 60px;
		}
		table{
			max-width:1200px;
			margin-bottom:50px;
			margin-left:auto;
			margin-right:auto;
			background-color: none;
			text-align:center;

			border-style: solid;
			border-width: 5px;
			border-color: white;
		}
		th{
			background-color: none;
			border:5px solid #FFFFFF;
			font-size: 30px;
			font-family: Helvetica;
			text-align: center;
			padding-top: 10px ;
			padding-bottom: 10px ;
		}
		td{
			border:2px solid white;
			font-size: 28px;
			font-family: Helvetica;
			text-align: center;
			padding-top: 5px ;
			padding-bottom: 5px ;
		}
		input[type=submit]{

			color: black;
			border: none;
			background: transparent;
			font-weight: 700;
			font-family: Helvetica;
			font-size: 20px;
			text-align: center;
			cursor: pointer;

		}

		input[type=submit]:hover, input[type=button]:hover{
			background-color: none;
			box-shadow: 0 5px none;
			transform: translateY(4px);
		}
		

		div{
			margin: auto;
			margin-top: 50px;
			padding-bottom: 1px;
			width: 70%;
			background-color: none;
		}

		body{
			width: 99%;
			height:98%;
			/* color:#457888; */
			font-family: Arial;
			background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			position: relative;
			animation: change 10s ease-in-out infinite;
		}

		@keyframes change {
			0%{
				background-position: 0 50%;
			}
			50%{
				background-position: 100% 50%;
			}
			100%{
				background-position: 0 50%;
			}
		}


		
	</style>
</head>
<body>

	<!-- <button onclick="topFunction()" id="myBtn" title="Go to top"></button> -->

	<!--Sort according to name in ascending/descending order-->
	<!--Sort according to UserID by default-->
	<div id="view" align="center">
		<br>
		<p><span style="text-decoration: underline;font-weight: 900;font-size: 55px">  Venue List </span></p>
		<form action="event-list-view.php" method="POST" style="font-size: 20px;">
		<table align="center" cellpadding="20px" cellspacing="6px">
			<tr>
				<th>No.</th>
				<th>Venue Name</th>
				<th>Info</th>
                <th>Edit</th>
			</tr>
			</form>
			<?php

                // if (isset($_POST['attendeeslist'])) {
                //     header('Refresh: 0; attendees-manage-view.php');

                // }


				if (isset($_POST['ascending'])) {
					$count=0;
					$conn = mysqli_connect($servername, $username, $password, $dbname);
					$read_user = "SELECT * FROM event ORDER BY evt_name ASC";
					$result_read_user = mysqli_query($conn, $read_user);
					if(mysqli_num_rows($result_read_user)>0){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							$count=$count+1;
							echo "<tr>";
							echo "<td>".$count."</td>";
							//echo "<td>".$row['eventID']."</td>";
							echo "<td>".$row['evt_name']."</td>";
							echo "<td>".$row['evt_datetime']."</td>";
							echo "<td>".$row['tkt_sold']."</td>";
                            echo "<td><input type='submit' name='attendeeslist' value='👁' style = 'font-size:40px'></td>";
							echo "<tr>";
						}
					}
				}
				else if (isset($_POST['descending'])) {
					$count=0;
					$conn = mysqli_connect($servername, $username, $password, $dbname);
					$read_user = "SELECT * FROM event ORDER BY evt_name DESC";
					$result_read_user = mysqli_query($conn, $read_user);
					if(mysqli_num_rows($result_read_user)>0){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
							$count=$count+1;
							echo "<tr>";
							echo "<td>".$count."</td>";
							//echo "<td>".$row['eventID']."</td>";
							echo "<td>".$row['evt_name']."</td>";
							echo "<td>".$row['evt_datetime']."</td>";
							echo "<td>".$row['tkt_sold']."</td>";
                            echo "<td><input type='submit' name='attendeeslist' value='👁' style = 'font-size:40px'></td>";
							echo "<tr>";
						}
					}
				}
				else{
					$count=0;
					$conn = mysqli_connect($servername, $username, $password, $dbname);
					$read_user = "SELECT * FROM venue ORDER BY venueID ASC";
					$result_read_user = mysqli_query($conn, $read_user);
					if(mysqli_num_rows($result_read_user)>0){
						while($row = mysqli_fetch_array($result_read_user, MYSQLI_ASSOC)){
                            $count=$count+1;
                            echo "<form action='venue-manage-edit.php' method='POST'>";
							echo "<tr>";
							echo "<td>".$count."</td>";
                            //echo "<td>".$row['eventID']."</td>";
                            echo "<input class ='venueID'  type='hidden' name='venueid' value='".$row['venueID']."'>";
							echo "<td>".$row['v_name']."</td>";
                            echo "<td>".$row['v_info']."</td>";
                            echo "<td><input type='submit' name='venuelist' value='✎' style = 'font-size:40px'></td>";
                            echo "</form>";
							echo "<tr>";
						}
					}
				}
			?>
		</table>
	</div>
</body>
</html>